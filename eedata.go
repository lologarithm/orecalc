package main

type Ore int

const (
	OreUnknown Ore = iota
	OreVeldspar
	OrePlagioclase
	OreScordite
	OreOmber
	OrePyroxeres
	OreKernite
	OreDarkOchre
	OreGneiss
	OreSpodumain
	OreHemorphite
	OreHedbergite
	OreJaspet
	OreCrokite
	OreBistot
	OreArkonor
	OreMercoxit
)

// OreFromName returns the Ore enum value for the common ore name.
func OreFromName(name string) Ore {
	switch name {
	case "Veldspar":
		return OreVeldspar
	case "Plagioclase":
		return OrePlagioclase
	case "Scordite":
		return OreScordite
	case "Omber":
		return OreOmber
	case "Pyroxeres":
		return OrePyroxeres
	case "Kernite":
		return OreKernite
	case "DarkOchre":
		return OreDarkOchre
	case "Gneiss":
		return OreGneiss
	case "Spodumain":
		return OreSpodumain
	case "Hemorphite":
		return OreHemorphite
	case "Hedbergite":
		return OreHedbergite
	case "Jaspet":
		return OreJaspet
	case "Crokite":
		return OreCrokite
	case "Bistot":
		return OreBistot
	case "Arkonor":
		return OreArkonor
	case "Mercoxit":
		return OreMercoxit
	}

	return OreUnknown
}

func (o Ore) String() string {
	switch o {
	case OreVeldspar:
		return "Veldspar"
	case OrePlagioclase:
		return "Plagioclase"
	case OreScordite:
		return "Scordite"
	case OreOmber:
		return "Omber"
	case OrePyroxeres:
		return "Pyroxeres"
	case OreKernite:
		return "Kernite"
	case OreDarkOchre:
		return "DarkOchre"
	case OreGneiss:
		return "Gneiss"
	case OreSpodumain:
		return "Spodumain"
	case OreHemorphite:
		return "Hemorphite"
	case OreHedbergite:
		return "Hedbergite"
	case OreJaspet:
		return "Jaspet"
	case OreCrokite:
		return "Crokite"
	case OreBistot:
		return "Bistot"
	case OreArkonor:
		return "Arkonor"
	case OreMercoxit:
		return "Mercoxit"
	}

	return "Unknown"
}

type Mineral int

const (
	MinUnknown Mineral = iota
	MinTritanium
	MinPyerite
	MinMexallon
	MinIsogen
	MinNocxium
	MinZydrine
	MinMegacyte
	MinMorphite
)

func (m Mineral) String() string {
	switch m {
	case MinTritanium:
		return "Tritanium"
	case MinPyerite:
		return "Pyerite"
	case MinMexallon:
		return "Mexallon"
	case MinIsogen:
		return "Isogen"
	case MinNocxium:
		return "Nocxium"
	case MinZydrine:
		return "Zydrine"
	case MinMegacyte:
		return "Megacyte"
	case MinMorphite:
		return "Morphite"
	}
	return "Unknown"
}

// MineralFromName returns the Mineral enum value for the common mineral name.
func MineralFromName(name string) Mineral {
	switch name {
	case "Tritanium":
		return MinTritanium
	case "Pyerite":
		return MinPyerite
	case "Mexallon":
		return MinMexallon
	case "Isogen":
		return MinIsogen
	case "Nocxium":
		return MinNocxium
	case "Zydrine":
		return MinZydrine
	case "Megacyte":
		return MinMegacyte
	case "Morphite":
		return MinMorphite
	}

	return MinUnknown
}

type ProcessSkill int

const (
	ProcessSkillUnknown ProcessSkill = iota
	ProcessSkillCommon
	ProcessSkillUncommon
	ProcessSkillSpecial
	ProcessSkillRare
	ProcessSkillPrecious
)

func SkillFromName(name string) ProcessSkill {
	switch name {
	case "Common":
		return ProcessSkillCommon
	case "Uncommon":
		return ProcessSkillUncommon
	case "Special":
		return ProcessSkillSpecial
	case "Rare":
		return ProcessSkillRare
	case "Precious":
		return ProcessSkillPrecious
	}
	return ProcessSkillUnknown
}

func (ps ProcessSkill) Name() string {
	switch ps {
	case ProcessSkillCommon:
		return "Common"
	case ProcessSkillUncommon:
		return "Uncommon"
	case ProcessSkillSpecial:
		return "Special"
	case ProcessSkillRare:
		return "Rare"
	case ProcessSkillPrecious:
		return "Precious"
	}
	return "Unknown"
}

var oreSkills = []ProcessSkill{
	OreVeldspar:    ProcessSkillCommon,
	OrePlagioclase: ProcessSkillCommon,
	OreScordite:    ProcessSkillCommon,
	OreOmber:       ProcessSkillUncommon,
	OrePyroxeres:   ProcessSkillUncommon,
	OreKernite:     ProcessSkillUncommon,
	OreDarkOchre:   ProcessSkillUncommon,
	OreGneiss:      ProcessSkillSpecial,
	OreSpodumain:   ProcessSkillSpecial,
	OreHemorphite:  ProcessSkillSpecial,
	OreHedbergite:  ProcessSkillRare,
	OreJaspet:      ProcessSkillRare,
	OreCrokite:     ProcessSkillRare,
	OreBistot:      ProcessSkillPrecious,
	OreArkonor:     ProcessSkillPrecious,
	OreMercoxit:    ProcessSkillPrecious,
}

// oreConversions is how many of each mineral comes out of 100 of the given ore.
var oreConversions = [][]float64{
	OreVeldspar:    {0, 125, 0, 0, 0, 0, 0, 0, 0},
	OrePlagioclase: {0, 15.3, 19.5, 29.1, 0, 0, 0, 0, 0},
	OreScordite:    {0, 48.6, 34.5, 0, 0, 0, 0, 0, 0},
	OreOmber:       {0, 180, 22.8, 0, 16.5, 0, 0, 0, 0},
	OrePyroxeres:   {0, 526, 168, 75, 0, 9, 0, 0, 0},
	OreKernite:     {0, 79.8, 0, 144, 14.4, 0, 0, 0, 0},
	OreDarkOchre:   {0, 288, 0, 0, 16.8, 12.9, 0, 0, 0},
	OreGneiss:      {0, 0, 264, 276, 55.2, 0, 0, 0, 0},
	OreSpodumain:   {0, 5910, 1122, 108, 18, 0, 0, 0, 0},
	OreHemorphite:  {0, 1650, 0, 0, 48, 3.9, 15, 0, 0},
	OreHedbergite:  {0, 0, 819, 0, 138, 2.7, 4.2, 0, 0},
	OreJaspet:      {0, 0, 0, 738, 0, 14.4, 16.8, 0, 0},
	OreCrokite:     {0, 11640, 0, 0, 0, 28.2, 28.8, 0, 0},
	OreBistot:      {0, 0, 1836, 0, 0, 0, 10.8, 23.7, 0},
	OreArkonor:     {0, 2640, 0, 300, 0, 0, 0, 31.2, 0},
	OreMercoxit:    {0, 0, 0, 0, 0, 0, 0, 0, 18},
}

var oreVolumes = []float64{
	OreVeldspar:    0.1,
	OrePlagioclase: 0.35,
	OreScordite:    0.15,
	OreOmber:       0.6,
	OrePyroxeres:   1.5,
	OreKernite:     1.2,
	OreDarkOchre:   1.6,
	OreGneiss:      2,
	OreSpodumain:   3.2,
	OreHemorphite:  3,
	OreHedbergite:  3,
	OreJaspet:      4,
	OreCrokite:     6.4,
	OreBistot:      6.4,
	OreArkonor:     6.6,
	OreMercoxit:    8,
}

// Convenience lists if you ever need to iterate all minerals or ores
var mineralList = []Mineral{MinTritanium, MinPyerite, MinMexallon, MinIsogen, MinNocxium, MinZydrine, MinMegacyte, MinMorphite}
var oreList = []Ore{OreVeldspar, OrePlagioclase, OreScordite, OreOmber, OrePyroxeres, OreKernite, OreDarkOchre, OreGneiss, OreSpodumain, OreHemorphite, OreHedbergite, OreJaspet, OreCrokite, OreBistot, OreArkonor, OreMercoxit}
