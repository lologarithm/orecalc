package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", convertPage)
	http.HandleFunc("/convert", convertAPI)
	http.HandleFunc("/oreforbp", oreFromBP)
	log.Fatalf("Server Done: %s", http.ListenAndServe(":6900", nil))
}
