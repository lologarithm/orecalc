package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("mysql", dsn("EveEchoes"))
	if err != nil {
		log.Printf("Error %s when opening DB\n", err)
		return
	}
	log.Printf("Connected to DB")
}

var (
	username = os.Getenv("sqluser")
	password = os.Getenv("sqlpass")
	hostname = os.Getenv("sqlhost")
)

func dsn(dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}

var oreValue []float64 = make([]float64, OreMercoxit+1)
var mineralValue []float64 = make([]float64, MinMorphite+1)

var bpmut = &sync.RWMutex{}
var bpCache = map[string][]int{}

func getMineralsFromBP(bp string) []int {
	bpmut.RLock()
	if v, ok := bpCache[bp]; ok {
		bpmut.RUnlock()
		return v
	}
	bpmut.RUnlock()

	minerals := make([]int, MinMorphite+1)
	readVals := make([]interface{}, MinMorphite) // no empty value at start, 1 longer

	// Hax: Make the array we are going to read into just pointers into the final destination.
	for k := range readVals {
		readVals[k] = &minerals[k+1]
	}

	rows, err := db.Query("SELECT Tritanium, Pyerite, Mexallon, Isogen, Nocxium, Zydrine, Megacyte, Morphite FROM blueprints WHERE Name=?", bp)
	if err != nil {
		log.Printf("Query Failed: %s", err)
	}
	for rows.Next() {
		err := rows.Scan(readVals...)
		if err != nil {
			log.Printf("Query Scan Failed: %s", err)
		}
	}
	bpmut.Lock()
	bpCache[bp] = minerals
	bpmut.Unlock()
	return minerals
}

func convert(skills []float64, ores []int) Ores {
	totalOutput := make([]int, MinMorphite+1)
	for k, v := range ores {
		if v == 0 {
			continue
		}
		for orekey, out := range oreToMinerals(v, Ore(k), skills) {
			totalOutput[orekey] += out
		}
	}
	return totalOutput
}

func oreToMinerals(quantity int, ore Ore, skills []float64) []int {
	quantity = quantity / 100
	processSkill := oreSkills[ore]
	eff := skills[processSkill]
	output := make([]int, MinMorphite+1)
	for i, v := range oreConversions[ore] {
		val := float64(quantity) * v * (eff / 30.0)
		output[i] = int(val)
	}
	return output
}

func oreFromMinerals(minerals []int, skills []float64) Ores {
	counts := make([]int, len(minerals))
	copy(counts, minerals)
	output := make(Ores, OreMercoxit+1) // long enough to handle all ores

	// Turns out that iterating forwards results in less m3.... weird
	for _, i := range mineralList {
		count := counts[i]
		if count > 0 {
			bestOre := volumeEffOre(i, skills)
			if bestOre == OreUnknown {
				log.Printf("No good ore found...wtf")
			}
			mineralPer100 := oreConversions[bestOre][i] * (skills[oreSkills[bestOre]] / 30.0)
			neededOre := (int(float64(count)/mineralPer100) + 1) * 100
			mineralsFromOre := oreToMinerals(neededOre, bestOre, skills)

			for min := range mineralsFromOre {
				if min == 0 || mineralsFromOre[min] == 0 {
					continue
				}
				counts[min] -= mineralsFromOre[min]
			}
			output[bestOre] += neededOre
		}
	}

	totalVol := 0.0
	for k, v := range output {
		if v == 0 {
			continue
		}
		totalVol += float64(v) * oreVolumes[k]
	}

	log.Printf("Output: %v, Total Volume: %.1f", output.String(), totalVol)

	return output
}

// volumeEffOre returns the most efficient ores for the given minerals set and skills.
func volumeEffOre(mineral Mineral, skills []float64) Ore {
	besteff := 0.0
	bestore := OreUnknown

	for _, ore := range oreList {
		if ore == OreUnknown {
			continue
		}
		minerals := oreToMinerals(100, ore, skills)
		eff := float64(minerals[mineral]) / oreVolumes[ore]
		if eff > besteff {
			// log.Printf("%s gives more %s per m3 (%.1f)", ore.String(), mineral.String(), eff)
			besteff = eff
			bestore = ore
		}
	}

	return bestore
}
