package main

import (
	"encoding/json"
	"strconv"
)

// These types exist to make json in/out easier/prettier

// Ores represents a collection of ore counts.
// Mostly used to give a convenience for printing and json conversion.
type Ores []int

// MarshalJSON implements the json marshalling interface so we get pretty json output.
func (o Ores) MarshalJSON() ([]byte, error) {
	return []byte(o.String()), nil
}

// UnmarshalJSON implements the json unmarshalling interface so we can accept pretty json.
func (o *Ores) UnmarshalJSON(data []byte) error {
	temp := make([]int, OreMercoxit+1)
	raw := map[string]int{}
	err := json.Unmarshal(data, &raw)
	for k, v := range raw {
		if v == 0 {
			continue
		}
		ok := OreFromName(k)
		if ok == OreUnknown {
			continue
		}
		temp[ok] = v
	}
	*o = temp
	return err
}

// String outputs our ores nice and pretty like a map without having to use a map
func (o Ores) String() string {
	values := ""
	first := true
	for k, v := range o {
		if v == 0 || k == 0 {
			continue
		}
		if !first {
			values += ", "
		} else {
			first = false
		}
		values += "\"" + Ore(k).String() + "\": " + strconv.Itoa(v)
	}
	return "{" + values + "}"
}

// Minerals represents a collection of mineral counts.
// Mostly used to give a convenience for printing and json conversion.
type Minerals []int

// MarshalJSON implements the json marshalling interface so we get pretty json output.
func (m Minerals) MarshalJSON() ([]byte, error) {
	return []byte(m.String()), nil
}

// UnmarshalJSON implements the json unmarshalling interface so we can accept pretty json.
func (m *Minerals) UnmarshalJSON(data []byte) error {
	temp := make([]int, MinMorphite+1)
	raw := map[string]int{}
	err := json.Unmarshal(data, &raw)
	for k, v := range raw {
		if v == 0 {
			continue
		}
		ok := MineralFromName(k)
		if ok == MinUnknown {
			continue
		}
		temp[ok] = v
	}
	*m = temp
	return err
}

// String outputs our minerals nice and pretty like a map without having to use a map
func (m Minerals) String() string {
	values := ""
	first := true
	for k, v := range m {
		if v == 0 || k == 0 {
			continue
		}
		if !first {
			values += ", "
		} else {
			first = false
		}
		values += "\"" + Mineral(k).String() + "\": " + strconv.Itoa(v)
	}
	return "{" + values + "}"
}
