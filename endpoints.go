package main

import (
	"net/http"
	"strings"
	"strconv"
	"log"
	"io/ioutil"
	"encoding/json"
)

func convertPage(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte(`<html>
		<form action="/convert" method="POST">
		<label for="Veldspar">Veldspar</label><input type="text" id="Veldspar" name="Veldspar" value=""><br>
		<label for="Plagioclase">Plagioclase</label><input type="text" id="Plagioclase" name="Plagioclase" value=""><br>
		<label for="Scordite">Scordite</label><input type="text" id="Scordite" name="Scordite" value=""><br>
		<label for="Omber">Omber</label><input type="text" id="Omber" name="Omber" value=""><br>
		<label for="Pyroxeres">Pyroxeres</label><input type="text" id="Pyroxeres" name="Pyroxeres" value=""><br>
		<label for="Kernite">Kernite</label><input type="text" id="Kernite" name="Kernite" value=""><br>
		<label for="DarkOchre">DarkOchre</label><input type="text" id="DarkOchre" name="DarkOchre" value=""><br>
		<label for="Gneiss">Gneiss</label><input type="text" id="Gneiss" name="Gneiss" value=""><br>
		<label for="Spodumain">Spodumain</label><input type="text" id="Spodumain" name="Spodumain" value=""><br>
		<label for="Hemorphite">Hemorphite</label><input type="text" id="Hemorphite" name="Hemorphite" value=""><br>
		<label for="Hedbergite">Hedbergite</label><input type="text" id="Hedbergite" name="Hedbergite" value=""><br>
		<label for="Jaspet">Jaspet</label><input type="text" id="Jaspet" name="Jaspet" value=""><br>
		<label for="Crokite">Crokite</label><input type="text" id="Crokite" name="Crokite" value=""><br>
		<label for="Bistot">Bistot</label><input type="text" id="Bistot" name="Bistot" value=""><br>
		<label for="Arkonor">Arkonor</label><input type="text" id="Arkonor" name="Arkonor" value=""><br>
		<label for="Mercoxit">Mercoxit</label><input type="text" id="Mercoxit" name="Mercoxit" value=""><br>
		<br>
		<label for="Common">Common</label><input type="text" id="Common" name="Common" value="30"><br>
		<label for="Uncommon">Uncommon</label><input type="text" id="Uncommon" name="Uncommon" value="30"><br>
		<label for="Special">Special</label><input type="text" id="Special" name="Special" value="30"><br>
		<label for="Rare">Rare</label><input type="text" id="Rare" name="Rare" value="30"><br>
		<label for="Precious">Precious</label><input type="text" id="Precious" name="Precious" value="30"><br>
		<input type="submit" value="Submit">
		</form>
		</html>`))
}

// returns skills, ores, minerals
func parseInputs(r *http.Request) ([]float64, []int, []int, string) {
	var skills []float64 = []float64{
		ProcessSkillCommon:   30,
		ProcessSkillUncommon: 30,
		ProcessSkillSpecial:  30,
		ProcessSkillRare:     30,
		ProcessSkillPrecious: 30,
	}
	parsedOres := make([]int, OreMercoxit+1)
	parsedMinerals := make([]int, MinMorphite+1)
	bp := ""
	if len(r.Header["Content-Type"]) == 0 {
		// w.Write([]byte(`{"error": "requires Content-Type header"}`))
		return nil, nil, nil, ""
	}
	contentType := r.Header["Content-Type"][0]
	if strings.Contains(contentType, "form") {
		r.ParseForm()
		// log.Printf("FORM: %#v", r.Form)
		for k, v := range r.Form {
			if len(v) == 0 {
				continue
			}
			sk := SkillFromName(k)
			if sk == ProcessSkillUnknown {
				ok := OreFromName(k)
				if ok == OreUnknown {
					continue
				}
				parsedOres[ok], _ = strconv.Atoi(v[0])
				continue
			}
			skills[sk], _ = strconv.ParseFloat(v[0], 64)
		}
	} else if strings.Contains(contentType, "json") {
		type JSONInput struct {
			Skills    map[string]float64
			Ores      Ores
			Minerals  Minerals
			Blueprint string
		}
		in := &JSONInput{}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Failed to read body: %s", err)
			return nil, nil, nil, ""
		}
		r.Body.Close()
		json.Unmarshal(body, in)
		for k, v := range in.Skills {
			if v == 0 {
				continue
			}
			sk := SkillFromName(k)
			skills[sk] = v
		}
		bp = in.Blueprint
		parsedOres = in.Ores
		parsedMinerals = in.Minerals
	}

	return skills, parsedOres, parsedMinerals, bp
}


func oreFromBP(w http.ResponseWriter, r *http.Request) {
	skills, _, minerals, bp := parseInputs(r)
	if bp != "" {
		minerals = getMineralsFromBP(bp)
	}

	log.Printf("Input: %v, %v", skills, minerals)
	ores := oreFromMinerals(minerals, skills)
	type output struct {
		Ore Ores
	}
	data, _ := json.Marshal(output{Ore: ores})
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func convertAPI(w http.ResponseWriter, r *http.Request) {
	skills, parsedOres, _, _ := parseInputs(r)
	totalOutput := convert(skills, parsedOres)

	log.Printf("Request\n\tin: %v / %v\n\tout: %v", skills, parsedOres, totalOutput)

	type output struct {
		Ore Ores
	}
	data, _ := json.Marshal(output{Ore: totalOutput})
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}